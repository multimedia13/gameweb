import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //Scaffold Widget
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter App"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.refresh))
          ],
        ),
        body: Center(
          child: Text(
              "Hello Flutter !",
              style: TextStyle(
                  fontSize: 32
              ),
          ),
        ),
      ),
    );
  }
}